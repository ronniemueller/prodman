# Improvements
- Retry to increment counter when an optimistic lock exception is thrown
- Currency Rates could be cached if real time values are not needed.
- More test are needed to test various error cases.
- Internal exceptions should not be given outside
- Currencies should not be floating point (double) 
- Supported Currencies should probably be in config file
- Error replies not consistent "timestamp": "Timestamp" or "Path" not always set.
- DB Setup with Liquibase or similar framework
- Values for Timeouts, Circuit Breaker and Bulkhead need to be set correctly.

# Assumptions
- Increment "View Counter" in good and error case.
- An error is returned if there is a problem with the FxService
