package com.galvanize.prodman.rest;

import com.galvanize.prodman.domain.Product;
import com.galvanize.prodman.model.FxResponse;
import com.galvanize.prodman.repository.ProductRepository;
import com.galvanize.prodman.service.FxService;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest()
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private FxService fxService;

    @Test
    @Tag("integration")
    void testGetProductWithCurrencyConversion() throws Exception {

        Product product = new Product();
        product.setId(1);
        product.setName("Name");
        product.setDescription("Description");
        product.setPrice(1.0);
        product.setViews(1);

        Mockito.when(productRepository.getById(Mockito.anyInt())).thenReturn(product);

        FxResponse fxResponse = new FxResponse();
        Map<String, Double> quotes = new HashMap<>();
        quotes.put("USDCAD", 2.0);
        fxResponse.setQuotes(quotes);

        Mockito.when(fxService.getQuotes()).thenReturn(fxResponse);

        mvc.perform(MockMvcRequestBuilders
                        .get("http://localhost:8080/api/products/1/?currency=CAD")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Name"))
                .andExpect(jsonPath("$.description").value("Description"))
                .andExpect(jsonPath("$.price").value(Double.valueOf("2.0")));
    }

    @Test
    @Tag("integration")
    void testGetProductInvalidId() throws Exception {

        Mockito.when(productRepository.getById(Mockito.anyInt())).thenThrow(new EntityNotFoundException());

        Mockito.when(fxService.getQuotes()).thenReturn(null);

        mvc.perform(MockMvcRequestBuilders
                        .get("http://localhost:8080/api/products/1/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.httpStatus").value(404))
                .andExpect(jsonPath("$.message").value("Invalid product id"));
    }

    // TODO: 07.12.21 More integration tests here
}
