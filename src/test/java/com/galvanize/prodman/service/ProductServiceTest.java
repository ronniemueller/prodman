package com.galvanize.prodman.service;

import com.galvanize.prodman.domain.Product;
import com.galvanize.prodman.model.FxResponse;
import com.galvanize.prodman.model.ProductDTO;
import com.galvanize.prodman.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class ProductServiceTest {

    private final ProductRepository productRepository = Mockito.mock(ProductRepository.class);
    private final FxService fxService = Mockito.mock(FxService.class);

    private final ProductService productService = new ProductService(productRepository, fxService);

    @Test
    public void testGetProduct() {
        Product product = new Product();
        product.setId(1);
        product.setName("Name");
        product.setDescription("Description");
        product.setPrice(1.0);
        product.setViews(1);

        when(productRepository.getById(anyInt())).thenReturn(product);

        FxResponse fxResponse = new FxResponse();
        Map<String, Double> quotes = new HashMap<>();
        quotes.put("USDEUR", 2.0);
        fxResponse.setQuotes(quotes);

        when(fxService.getQuotes()).thenReturn(fxResponse);

        ProductDTO productDTO = productService.get(1,"EUR");
        assertEquals(1,productDTO.getId());
        assertEquals("Name",productDTO.getName());
        assertEquals("Description",productDTO.getDescription());
        assertEquals(2.0, productDTO.getPrice());
    }

    // TODO: 07.12.21 More Service level tests here
}
