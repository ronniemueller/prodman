package com.galvanize.prodman.service;

import com.galvanize.prodman.model.FxResponse;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Service
public class FxService {

    private static final String SUPPORTED_CURRENCIES = "USD,CAD,EUR,GBP";

    @Value("${fx.api.url}")
    private String fxApiUrl;

    @Value("${fx.api.key}")
    private String fxApiKey;

    private final RestTemplate restTemplate;

    public FxService(final RestTemplateBuilder restTemplateBuilder,
                     @Value("${fx.rest.connectTimeoutMs}") final long fxConnectTimeoutMs,
                     @Value("${fx.rest.readTimeoutMs}") final long fxReadTimeoutMs) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(Duration.ofMillis(fxConnectTimeoutMs))
                .setReadTimeout(Duration.ofMillis(fxReadTimeoutMs))
                .build();
    }

    @CircuitBreaker(name = "FxService")
    @Bulkhead(name = "FxService")
    public FxResponse getQuotes() {
        String endPoint = String.format("%s?access_key=%s&currencies=%s&format=1", fxApiUrl, fxApiKey, SUPPORTED_CURRENCIES);
        return restTemplate.getForObject(endPoint, FxResponse.class);
    }
}
