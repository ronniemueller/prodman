package com.galvanize.prodman.service;

import com.galvanize.prodman.domain.Product;
import com.galvanize.prodman.model.FxResponse;
import com.galvanize.prodman.model.ProductDTO;
import com.galvanize.prodman.repository.ProductRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;


@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final FxService fxService;

    public ProductService(final ProductRepository productRepository, final FxService fxService) {
        this.productRepository = productRepository;
        this.fxService = fxService;
    }

    public Integer create(final ProductDTO productDTO) {
        final Product product = new Product();
        mapToEntity(productDTO, product);
        return productRepository.save(product).getId();
    }

    @Transactional
    public ProductDTO get(final Integer id, final String currency) {
        try {
            final Product product = productRepository.getById(id);

            final ProductDTO productDTO = new ProductDTO();
            mapToDTO(product, productDTO);
            product.setViews(product.getViews() + 1);
            productRepository.save(product);

            if (currency != null) {
                Double conversionRate = getConversionRate(currency);
                productDTO.setPrice(productDTO.getPrice() * conversionRate);
            }

            return productDTO;

        } catch (EntityNotFoundException ex) {
            throw new ServiceException("Invalid product id", HttpStatus.NOT_FOUND);
        }
    }

    private Double getConversionRate(String currency) {
        FxResponse fxResponse;
        try {
            fxResponse = fxService.getQuotes();
        } catch (Exception ex) {
            throw new ServiceException("Currency temporarily unavailable", HttpStatus.SERVICE_UNAVAILABLE);
        }

        Double conversionRate = fxResponse.getQuotes().get("USD" + currency.toUpperCase());
        if (conversionRate == null) {
            throw new ServiceException("Invalid currency", HttpStatus.BAD_REQUEST);
        }
        return conversionRate;
    }

    public void delete(final Integer id) {
        productRepository.deleteById(id);
    }

    private Product mapToEntity(final ProductDTO productDTO, final Product product) {
        product.setName(productDTO.getName());
        product.setDescription(productDTO.getDescription());
        product.setPrice(productDTO.getPrice());
        product.setViews(0);
        product.setDeleted(false);
        return product;
    }

    private ProductDTO mapToDTO(final Product product, final ProductDTO productDTO) {
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setDescription(product.getDescription());
        productDTO.setPrice(product.getPrice());
        return productDTO;
    }

}
