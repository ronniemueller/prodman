package com.galvanize.prodman.rest;

import com.galvanize.prodman.model.ProductDTO;
import com.galvanize.prodman.service.ProductService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Size;


@RestController
@RequestMapping(value = "/api/", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class ProductController {

    private final ProductService productService;

    public ProductController(final ProductService productService) { this.productService = productService; }

    @GetMapping("products/{id}")
    public ResponseEntity<ProductDTO> get(@PathVariable int id, @Param("currency") @Size(min = 3, max = 3) String currency) {
        ProductDTO productDTO = productService.get(id, currency);
        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }
}
